//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "SimpleApp.h"
#include <algorithm> // random_shuffle

Define_Module(SimpleApp);

// return random number between 0 and 5 inclusive
//int randDelay = intuniform(1, 10);
//double uniform(a, b); between [a,b)

SimpleApp::SimpleApp() {
    selfMessage = nullptr;
    downloadTimes = nullptr;
    delete [] downloadTimes;
    EV_ERROR << "SimpleApp constructor\n";
}

SimpleApp::~SimpleApp() {
    cancelAndDelete(selfMessage);
}

void SimpleApp::initialize(int stage)
{
    cSimpleModule::initialize(stage);

    if (stage == 0) {

        timeEntered = simTime().dbl();

        // parameters
        requestFreq = par("requestFreq");
        maxGenerations = par("maxGenerations");
        int newDataFreq = par("newDataFreq");

        // gates
        fromNetworkCoding = findGate("fromNetworkCoding");
        toNetworkCoding = findGate("toNetworkCoding");

        // note the times (if simulation ends with endTime == -1,
        // car never finished
        startTime = simTime();
        endTime = -1;
        selfMessagesRecd = 0;

        // data download init
        numTotalChunks = par("numTotalChunks");
        recdAllData = false;
        numRequests = 0;

        // data won't appear until the RSU sends it
        totalPackets = maxGenerations * numTotalChunks;
        timesExpecting = new double[totalPackets];
        for (int i = 0;  i < totalPackets; i++) {
            int generation = i / numTotalChunks;
            double timeWillBeSent = generation * newDataFreq;
            timesExpecting[i] = timeWillBeSent;
        }

        // -1 when we haven't recd all data
        downloadTimes = new double[totalPackets];
        for (int i = 0; i < totalPackets; i++) {
            downloadTimes[i] = -1;
        }

        // still need all data
        for (int i = 0; i < maxGenerations * numTotalChunks; i++) {
            chunksNeeded.insert(to_string(i));
        }

        // send a message after 1 sec to start doing things
        lastRequestTime = 0;
//        selfMessage = new cMessage("self");
//        scheduleAt(simTime()+1, selfMessage);
    }
}

void SimpleApp::handleMessage(cMessage *msg)
{
    // check your state after repeatBeaconDelay seconds
    if (msg->isSelfMessage()) {
        selfMessagesRecd++;

        lastRequestTime = simTime().dbl();
        // true means I haven't recd all data, so re-send request
        if (broadcastRequest()) {
            // cancel first to re-schedule
            if (msg->isScheduled()) {
                cancelEvent(msg);
            }
            scheduleAt(simTime()+requestFreq, msg);
        }

        // false means you've received all data, so stop requesting
        else {
            cancelAndDelete(msg);
        }
    }


    // receive anything that isn't a self message
    else {
        WaveShortMessage* wsm = dynamic_cast<WaveShortMessage*>(msg);
        if (wsm != nullptr) {
            // at this point, you've recd either data or a request for data
            int psid = wsm->getPsid();
            if (psid == DATA_PSID) {
                // process data you receive
                EV_INFO << "Recd data\n";
                collectData(wsm->getWsmData());
            }

            else if (psid == REQUEST_PSID) {
                // respond to requests
                EV_INFO << "Recd request\n";
                handleDataRequest(wsm->getWsmData());
            }

            else {
                EV_ERROR << "PROBLEM: recd message with bad psid:" << psid << '\n';
            }
        }

        // problem if you receive a message that isn't a WaveShortMessage
        else {
            EV_ERROR << "PROBLEM: received some message that isn't a WSM\n";
        }
    }
}


// if a vehicle is requesting data, you just respond with
// all of the data that you have
void SimpleApp::handleDataRequest(string wsmdata)
{
    vector<string> toSend = getRequestedData(wsmdata);
    random_shuffle(toSend.begin(), toSend.end()); // don't send out data in the same order each time

    for (int i = 0; i < toSend.size(); i++) {
        // only send out the ones that you have
        if (chunksReceived.find(toSend[i]) != chunksReceived.end()) {

            WaveShortMessage* wsm = new WaveShortMessage();
            wsm->setWsmData(toSend[i].c_str());
            wsm->setPsid(DATA_PSID);
            wsm->setSenderAddress(-1);
            send(wsm, toNetworkCoding);
        }
    }
}

void SimpleApp::collectData(string data)
{
    // do nothing if you have all the data
    if (!recdAllData) {
        // as long as it's a real chunk and we haven't received that one yet,
        // add it to your buffer
        if (isNumber(data) && chunksReceived.find(data) == chunksReceived.end()) {
            int packet = stoi(data);
            downloadTimes[packet] = simTime().dbl() - timesExpecting[packet];

            chunksReceived.insert(data);

            // have you received all chunks?
            recdAllData = chunksReceived.size() == totalPackets;
        }
    }
}

    // returns true if a request was made
    // or false if not (all data received)
bool SimpleApp::broadcastRequest()
{
    // if downloadTime has been changed, you have all the data
    if (recdAllData) {
        return false;
    }

    // broadcast a request after a small amount of random time (avoid collisions)
    WaveShortMessage* wsm = new WaveShortMessage();

    string dataStillNeed = getDataNotReceived();
    wsm->setWsmData(dataStillNeed.c_str());

    wsm->setPsid(REQUEST_PSID);

    // indicate it hasn't been sent on the network yet
    wsm->setSenderAddress(-1);

    double delay = uniform(0, 1);
    sendDelayed(wsm, simTime() + delay, toNetworkCoding);

    numRequests++;

    return true;
}

void SimpleApp::finish()
{
    cSimpleModule::finish();

    timeLeft = simTime().dbl();

    recordScalar("timeEnteredSim", timeEntered);
    recordScalar("timeLeftSim", timeLeft);

    recordScalar("numSelfMessagesRecd", selfMessagesRecd);

    for (int i = 0; i < numTotalChunks*maxGenerations; i++) {
        string s = "downloadTime_" + to_string(i);
        recordScalar(s.c_str(), downloadTimes[i]);
    }

    recordScalar("numRequests", numRequests);
}
