/*
 * NetworkCoding.cc
 *
 *  Created on: Dec 7, 2018
 *      Author: trevor
 */


//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#include "NetworkCoding.h"
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <numeric>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <iostream>
#include <typeinfo>

using namespace std;

Define_Module(NetworkCoding);

NetworkCoding::NetworkCoding()
{

}

NetworkCoding::~NetworkCoding() {
    delete [] chunkBuffer;
    delete [] packetBuffer;
}


void NetworkCoding::initialize(int stage)
{
    if (stage == 0) {
        toApplication = findGate("toApplication");
        fromApplication = findGate("fromApplication");

        fromLower = findGate("lowerLayerIn"); // not super sure about this
        toLower = findGate("lowerLayerOut");

        doNetworkCoding = par("doNetworkCoding").boolValue();
        maxGenerations = par("maxGenerations");
        redundancy = par("redundancy");
        numChunksPerEncoding = par("numChunksPerEncoding");

        chunkBuffer = new vector<string>[maxGenerations];
        packetBuffer = new vector<string>[maxGenerations];

        field_size = 16;
        decode_prob = 1 - 1 / pow(2, field_size);

        timeOfLastChunkFromApplication = -1;
    }
}

void NetworkCoding::handleMessage(cMessage *msg)
{
    this->isParked = false;
    WaveShortMessage* wsm = dynamic_cast<WaveShortMessage*>(msg);

    if (wsm != nullptr) {

        int gateid = wsm->getArrivalGateId();
        int psid = wsm->getPsid();

        // if the message came down from the application,
        // then it's data that we know about and should be
        // encoding and sending out
        if (gateid == fromApplication) {

            // if the application is requesting data, just pass that request along
            if (psid == REQUEST_PSID) {
                WaveShortMessage* sendmsg = wsm->dup();
                populateWSM(sendmsg);
                sendmsg->setUserPriority(1);
                send(sendmsg, toLower);
            }

            // if the application is sending data, then encode and send
            else if (psid == DATA_PSID) {
                if (doNetworkCoding) {
                    timeOfLastChunkFromApplication = simTime().dbl();
                    encodeAndSend(wsm);
                } else {
                    WaveShortMessage* sendmsg = wsm->dup();
                    populateWSM(sendmsg);
                    sendmsg->setUserPriority(1);
                    send(sendmsg, toLower);
                }

            }

            // ERROR
            else {
                EV_ERROR << "ERROR - from application, not request nor data but " <<
                        wsm->getPsid();
            }
        }

        else {
            EV_ERROR << "ERROR - not from lower nor application but " <<
                    gateid << " with data " << wsm->getWsmData() << '\n';
            EV_ERROR << "Appl gateid " << fromApplication << " Lower gateid " << fromLower << '\n';
        }
    }
}


// if the message came up from the decision maker
// it is potentially an encoded message ... depends
// on if it came from the server or not
void NetworkCoding::onWSM(WaveShortMessage* wsm) {
    findHost()->getDisplayString().updateWith("r=16,green");

    int psid = wsm->getPsid();

    // if it came from the server or it's a data request,
    // just pass it up to the application
    // so that it can add it to the data that it has
    if (psid == REQUEST_PSID) {
        send(wsm, toApplication);
    }

    // if it came from anyone else (a vehicle), it's encoded so we need to decode
    // and pass up to the application so it can add it to the data it has
    else if (psid == DATA_PSID) {
        if (doNetworkCoding) {
            decodeAndPassUp(wsm);
        } else {
            send(wsm, toApplication);
        }
    }

    // ERROR
    else {
        EV_ERROR << "ERROR - from decision maker, not request or data " <<
                wsm->getWsmData();
    }
}


void NetworkCoding::encodeAndSend(WaveShortMessage* wsm)
{
    // Obtain the wsmData and pushback into chunkBuffer
    string wsmData = wsm->getWsmData();
    int gen = wsm->getGeneration();
    chunkBuffer[gen].push_back(wsmData);

    // when we have received enough data, we perform the encoding
    if (chunkBuffer[gen].size() > numChunksPerEncoding) {

        // put the chunks in here for now, then make a string out
        // of them later
        vector<string> encodedPacketChunks;

        // send out encoded packets, conifigurable by the desired redundancy.
        for (int i=0; i < redundancy; i++){

            // Obtain 4 different elements randomly from chunkbuffer and pushback into encodedPacket
            unordered_set<int> addedAlready;
            while (addedAlready.size() < numChunksPerEncoding) {
                int randNum = (int)uniform(0, chunkBuffer[gen].size()) % chunkBuffer[gen].size();
                if (addedAlready.find(randNum) == addedAlready.end()) {
                    addedAlready.insert(randNum);
                    encodedPacketChunks.push_back(chunkBuffer[gen][randNum]);
                }
            }

            // Convert the vector of strings into one string
            string stringEncodedPacket = vectorToString(encodedPacketChunks);
            encodedPacketChunks.clear();

            // set the wsmData and broadcast
            WaveShortMessage* sendthis = wsm->dup();
            sendthis->setWsmData(stringEncodedPacket.c_str());
            sendthis->setGeneration(gen);
            populateWSM(sendthis);
            sendthis->setUserPriority(1);
            send(sendthis, toLower);
        }

        // clear this so that when you get more data from the application, you aren't
        // reencoding things you've already sent out
        chunkBuffer[gen].clear();
    }

    // if you don't have enough to encode, wait a very small amount of time (0.5s) to see if
    // the application layer is going to send you more packets. If after that time, it
    // still hasn't passed you numChunksPerEncoding chunks, then just send them
    // individually ... this shouldn't happen very often, only is the application isn't
    // sending very much data
    else if (simTime().dbl() - timeOfLastChunkFromApplication > 0.5) {
        for (int i = 0; i != chunkBuffer[gen].size(); i++) {
            WaveShortMessage* w = wsm->dup();
            w->setWsmData(chunkBuffer[gen][i].c_str());
            w->setGeneration(0);
            populateWSM(w);
            sendDelayedDown(w, simTime()+uniform(0, 1));
        }
    }
}

void NetworkCoding::decodeAndPassUp(WaveShortMessage* wsm)
{
    // arbitrarily small prob it's not linearly independent of the rest
    if (uniform(0, 1) > decode_prob) {
        return;
    }

    // Obtain the wsmdata from the msg data
    const char* wsmData = wsm->getWsmData();
    string stringWsm(wsmData);
    int gen = wsm->getGeneration();

    // If not encoded just send
    if (stringWsm.find(',') == string::npos){
        send(wsm, toApplication);
    }

    // If encoded, we would need to pull the chunks from packets and send toApplication one-by-one
    else {

        // Packet buffer to make sure we have enough packets before decoding
        packetBuffer[gen].push_back(stringWsm);

        // if we receive sufficient packets, we can start decoding
        if (packetBuffer[gen].size() >= numChunksPerEncoding){

            // loop to go through chunks inside packetBuffer and pull data
            for (int i=0; i<packetBuffer[gen].size(); i++){
                vector<string> chunks = stringToVector(packetBuffer[gen][i]);

                for (int i = 0; i < chunks.size(); i++) {
                    WaveShortMessage* h = wsm->dup();
                    h->setWsmData(chunks[i].c_str());
                    send(h, toApplication);
                }
            }

            // erase the packet buffer when the chunks are pulled out and sent toApplication already
            // this is an interesting idea because if we erase them, then the network coding never
            // remembers what chunks have been received in the past, which I don't think is a bad
            // thing
            packetBuffer[gen].clear();
        }
    }
}

























