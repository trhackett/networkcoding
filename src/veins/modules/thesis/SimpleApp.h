//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __VEINS_SIMPLEAPP_H_
#define __VEINS_SIMPLEAPP_H_

#include "veins/modules/application/ieee80211p/BaseWaveApplLayer.h"
#include "veins/modules/messages/WaveShortMessage_m.h"
#include "veins/modules/thesis/SimpleDefs.h"

#include <omnetpp.h>

using namespace omnetpp;

class SimpleApp : public cSimpleModule
{
  public:
    SimpleApp();
    virtual ~SimpleApp();
  protected:
    virtual void initialize(int);
    virtual void handleMessage(cMessage *msg);
    virtual void finish();

    bool broadcastRequest();
    string getDataNotReceived();
    vector<string> getRequestedData(string);
    void collectData(string);
    bool isNumber(string);
    void handleDataRequest(string);

    // gates
    int toNetworkCoding;
    int fromNetworkCoding;

  private:
    simtime_t startTime;
    simtime_t endTime;

    uint32_t selfMessagesRecd;

    cMessage* selfMessage;

        // data download
    double* timesExpecting;
    double* downloadTimes;
    double lastRequestTime;
    bool recdAllData;
    int numRequests;

    int numTotalChunks;
    unordered_set<string> chunksNeeded;
    unordered_set<string> chunksReceived;

        // parameters

    // if haven't received all data, wait this amount of
    // time before re-broadcasting request for data.
    int requestFreq;
    int maxGenerations;
    int totalPackets;

    double timeEntered;
    double timeLeft;
};

#endif

// out of the data that you haven't received,
// build up a string listing all the chunks
// "3,6,10,44,2," <- looks like this
string SimpleApp::getDataNotReceived() {
    string notrecd = { };

    unordered_set<string>::iterator it = chunksNeeded.begin();

    for (; it != chunksNeeded.end(); ++it) {
        if (chunksReceived.find(*it) == chunksReceived.end()) {
            notrecd += *it + ",";
        }
    }

    return notrecd;
}

// string manipulation function that pulls the individual
// chunks out of the list of chunks not received by the
// requesting vehicle
vector<string> SimpleApp::getRequestedData(string data) {
    vector<string> ret;

    while (!data.empty()) {

        int commaindex = data.find(',');
        string num = data.substr(0, commaindex);
        data = data.substr(commaindex + 1, data.size());

        ret.push_back(num);
    }

    return ret;
}

// simple boolean function that checks that the data is just a single
// integer in string form
bool SimpleApp::isNumber(string s) {
    for (unsigned int i = 0; i < s.size(); i++) {
        if (!isdigit(s[i])) {
            return false;
        }
    }
    return true;
}

