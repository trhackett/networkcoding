/*
 * SimpleRSU.cc
 *
 *  Created on: Dec 7, 2018
 *      Author: trevor
 */




#include "veins/modules/thesis/SimpleRSU.h"

Define_Module(SimpleRSU);

void SimpleRSU::initialize(int stage) {
    if (stage == 0) {
        generation = 0;
        numTotalChunks = par("numTotalChunks");
        maxGenerations = par("maxGenerations");
        newDataFreq = par("newDataFreq");

        fromLower = findGate("lowerLayerIn"); // not super sure about this
        toLower = findGate("lowerLayerOut");

        selfmessage = new cMessage("self");
        scheduleAt(simTime()+5, selfmessage);
    }
}

void SimpleRSU::handleMessage(cMessage* msg) {
    if (!msg->isSelfMessage()) {
        return;
    }

    sendMoreData();

    if (msg->isScheduled()) {
        cancelEvent(msg);
    }

    // every 5 minutes, send another generation of data
    scheduleAt(simTime() + newDataFreq, msg);
}

void SimpleRSU::sendMoreData() {
    // don't do anything if we're beyond the max number
    // of generations
    if (generation >= maxGenerations) {
        return;
    }

    generation++;

    for (int i = 0; i < numTotalChunks; i++) {
        string data = to_string(i);
        WaveShortMessage* wsm = new WaveShortMessage;
        populateWSM(wsm);
        wsm->setWsmData(data.c_str());
        wsm->setPsid(DATA_PSID);
        wsm->setGeneration(generation);
        wsm->setUserPriority(1);
        sendDelayed(wsm, simTime() + uniform(0, 1), toLower);
    }
}

void SimpleRSU::onWSM(WaveShortMessage* wsm) {
    EV_INFO << "Received message from lower\n";
    if (wsm->getPsid() == REQUEST_PSID) {
        EV_INFO << "responding to data request\n";

//        vector<string> requested = getRequestedData(wsm->getWsmData());

        /*
        // respond with a packet containing each piece of data individually
        for (unsigned int i = 0; i < requested.size(); i++) {
            WaveShortMessage* wsm = new WaveShortMessage();
            populateWSM(wsm);

            wsm->setWsmData(requested[i].c_str());

            wsm->setPsid(DATA_PSID);

            sendDown(wsm);
        }
        */
    }
}
