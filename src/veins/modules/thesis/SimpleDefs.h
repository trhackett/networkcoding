/*
 * SimpleDefs.h
 *
 *  Created on: Dec 7, 2018
 *      Author: trevor
 */

#ifndef SRC_VEINS_MODULES_THESIS_SIMPLEDEFS_H_
#define SRC_VEINS_MODULES_THESIS_SIMPLEDEFS_H_

#include <vector>
#include <string>
#include <unordered_set>
using namespace std;


// psids
#define REQUEST_PSID 301
#define DATA_PSID    302

// chunks passed around
struct chunk {
    int number;      // type of data is just integer
    int generation;

};

#endif /* SRC_VEINS_MODULES_THESIS_SIMPLEDEFS_H_ */
