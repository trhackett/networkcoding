/*
 * SimpleRSU.h
 *
 *  Created on: Dec 7, 2018
 *      Author: trevor
 */

#ifndef SRC_VEINS_MODULES_THESIS_SIMPLERSU_H_
#define SRC_VEINS_MODULES_THESIS_SIMPLERSU_H_


#include "veins/modules/application/ieee80211p/BaseWaveApplLayer.h"
#include "veins/modules/thesis/SimpleDefs.h"

class SimpleRSU : public BaseWaveApplLayer {
public:
    void initialize(int);
protected:
    virtual void onWSM(WaveShortMessage* wsm);
    vector<string> getRequestedData(string);
    virtual void handleMessage(cMessage*) override;

private:
    cMessage* selfmessage;
    void sendMoreData();
    int generation;
    int numTotalChunks;
    int maxGenerations;
    int newDataFreq;

    // gates
    int fromLower;
    int toLower;
};


#endif /* SRC_VEINS_MODULES_THESIS_SIMPLERSU_H_ */

// string manipulation function that pulls the individual
// chunks out of the list of chunks not received by the
// requesting vehicle
vector<string> SimpleRSU::getRequestedData(string data) {
    vector<string> ret;

    while (!data.empty()) {

        int commaindex = data.find(',');
        string num = data.substr(0, commaindex);
        data = data.substr(commaindex + 1, data.size());

        ret.push_back(num);
    }

    return ret;
}
