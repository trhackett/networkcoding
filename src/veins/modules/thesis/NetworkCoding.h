/*
 * NetworkCoding.h
 *
 *  Created on: Dec 7, 2018
 *      Author: trevor
 */

#ifndef SRC_VEINS_MODULES_THESIS_NETWORKCODING_H_
#define SRC_VEINS_MODULES_THESIS_NETWORKCODING_H_

#include "veins/modules/application/ieee80211p/BaseWaveApplLayer.h"
#include "veins/modules/thesis/SimpleDefs.h"
#include <omnetpp.h>
using namespace omnetpp;

class NetworkCoding : public BaseWaveApplLayer
{
public:
    NetworkCoding();
    virtual ~NetworkCoding();
protected:
  virtual void initialize(int);
  virtual void handleMessage(cMessage *msg);
  virtual void onWSM(WaveShortMessage*);

  int toApplication;
  int fromApplication;

  int toLower;
  int fromLower;

  bool doNetworkCoding;

private:
  int field_size;
  double decode_prob;
  int numChunksPerEncoding;
  int maxGenerations;
  int redundancy;
  // on vector per generation
  vector<string>* chunkBuffer;
  vector<string>* packetBuffer;
  double timeOfLastChunkFromApplication;


  /*
   * These functions do the decoding and encoding
   * and return the number of chunks sent up/ passed down
   */
  void encodeAndSend(WaveShortMessage*);
  void decodeAndPassUp(WaveShortMessage*);
  string vectorToString(vector<string>&);
  vector<string> stringToVector(string);
};



#endif /* SRC_VEINS_MODULES_THESIS_NETWORKCODING_H_ */

string NetworkCoding::vectorToString(vector<string>& v) {
    string ret = {};
    for (int i = 0; i != v.size(); i++) {
        ret += v[i] + ",";
    }
    return ret;
}

vector<string> NetworkCoding::stringToVector(string encodedData)
{
    vector<string> ret;
    int i = 0;
    while (i < encodedData.size()) {
        string s = {};
        while (encodedData[i] != ',' && i < encodedData.size()) {
            s += encodedData[i];
            i++;
        }
        ret.push_back(s);
        i++; // pass the commas
    }
    return ret;
}
